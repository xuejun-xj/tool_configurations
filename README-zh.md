# tool-configurations

> 语言 | [English](https://gitlab.com/xuejun-xj/tool_configurations/-/blob/main/README.md) / [中文](https://gitlab.com/xuejun-xj/tool_configurations/-/blob/main/README-zh.md)

这个仓库包含了我个人使用的一些工具配置。

- [tmux](https://gitlab.com/xuejun-xj/tool_configurations/-/tree/main/tmux/)
- [bash](https://gitlab.com/xuejun-xj/tool_configurations/-/tree/main/bash/)
- [vim](https://gitlab.com/xuejun-xj/tool_configurations/-/tree/main/vim/)
