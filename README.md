# tool-configurations

> Language | [English](https://gitlab.com/xuejun-xj/tool_configurations/-/blob/main/README.md) / [中文](https://gitlab.com/xuejun-xj/tool_configurations/-/blob/main/README-zh.md)

This repository contains my personal Configurations of several tools.

- [tmux](https://gitlab.com/xuejun-xj/tool_configurations/-/tree/main/tmux/)
- [bash](https://gitlab.com/xuejun-xj/tool_configurations/-/tree/main/bash/)
- [vim](https://gitlab.com/xuejun-xj/tool_configurations/-/tree/main/vim/)
